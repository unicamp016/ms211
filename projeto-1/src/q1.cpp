/* Author: Rafael Figueiredo Prudencio */

#include <iostream>
#include <array>

using namespace std;

/* Prints bit representation of a given floating point number */
void print_bits (float val)
{
    int *val_int = (int *) &val;
    int buf, bit;

    for (int i = 31; i >= 0; i--) {
      buf = *val_int & (1 << i);
      if (buf) bit = 1;
      else bit = 0;
      cout << bit << ' ';
    }
    cout << endl;
}

/* Calculates precision of given type considering initial param val*/
template <class T>
T precision(T val) {
  T A = 1;
  T s = val + A;
  while (s > val) {
    A = A/2;
    s = val + A;
  }
  return A*2;
}

int main() {
  int n = 9;
  float val_f[n] = {1, 10, 17, 100, 184, 1000, 1575, 10000, 17893};
  double val_d[n] = {1, 10, 17, 100, 184, 1000, 1575, 10000, 17893};

  /*Itens (a) e (c) */
  for (int i = 1; i < n; i++) {
    cout << "Para VAL = " << val_f[i] << " temos:" << endl
         << "\tPrecisão simples: " << precision<float>(val_f[i]) << endl
         << "\tPrecisão dupla: " << precision<double>(val_d[i]) << endl;
  }

  return 0;
}
