/* Author: Rafael Figueiredo Prudencio */

#include "q3.hpp"

void mult(Matriz& A, Matriz& B, Matriz& C) {
  if (A.cols != B.rows) return;

  for(int i = 0; i < A.rows; i++) {
    for(int j = 0; j < B.cols; j++) {
      for(int k = 0; k < A.cols; k++) {
        C.data[i][j] += A.data[i][k]*B.data[k][j];
      }
    }
  }
}

void init_solucao(Matriz& x, int n) {
  for (int k = 1; k <= n; k++) {
    x.data[k-1][0] = 4.0*(k*(n+1)-k*k)/((n+1)*(n+1));
  }
}

void init_sistema(Matriz& A, Matriz& b, int n) {
  float b_i;

  /*Inicializa matriz de coeficientes*/
  A.data[0][0] = 2;
  A.data[0][1] = -1;
  A.data[n-1][n-1] = 2;
  A.data[n-1][n-2] = -1;
  for (int i = 1; i < n-1; i++) {
    A.data[i][i-1] = -1;
    A.data[i][i] = 2;
    A.data[i][i+1] = -1;
  }

  /* Inicializa matriz de valores */
  b_i = 8.0/((n+1)*(n+1));
  for (int i = 0; i < n; i++) {
    b.data[i][0] = b_i;
  }

}

void fatorar(Matriz A, Matriz& L, Matriz& U) {
  float a_ii, m_ji;

  /* Inicializa L como a matriz identidade */
  L.I();

  for (int i = 0; i < A.rows-1; i++) {
    a_ii = A.data[i][i];
    for (int j = i+1; j < A.rows; j++) {
      m_ji = A.data[j][i]/a_ii;
      L.data[j][i] = m_ji;
      for (int k = 0; k < A.cols; k++) {
        A.data[j][k] = A.data[j][k] - m_ji*A.data[i][k];
      }
    }
  }

  U = A.copy();
}

void fatorar_tridiagonal(Matriz A, Matriz& L, Matriz& U) {
  float a_ii, m_i;

  /* Inicializa L como a matriz identidade */
  L.I();

  for (int i = 0; i < A.rows-1; i++) {
    a_ii = A.data[i][i];
    m_i = A.data[i+1][i]/a_ii;
    L.data[i+1][i] = m_i;
    for (int k = 0; k < A.cols; k++) {
      A.data[i+1][k] = A.data[i+1][k] - m_i*A.data[i][k];
    }
  }

  U = A.copy();
}

void resolver_triangular(Matriz A, Matriz& x, Matriz b, int tipo) {
  switch (tipo) {
    case UPPER:
      for (int i = A.rows-1; i >= 0; i--) {
        x.data[i][0] = b.data[i][0];
        for (int j = A.cols-1; j > i; j--) {
          x.data[i][0] -= A.data[i][j]*x.data[j][0];
        }
        x.data[i][0] /= A.data[i][i];
      }
      break;
    case LOWER:
      for (int i = 0; i < A.rows; i++) {
        x.data[i][0] = b.data[i][0];
        for (int j = 0; j < i; j++) {
          x.data[i][0] -= A.data[i][j]*x.data[j][0];
        }
        x.data[i][0] /= A.data[i][i];
      }
      break;
  }
}

double calcular_erro(Matriz& x, Matriz& x_a) {
  double erro;
  for (int i = 0; i < x.rows; i++) {
     erro += (x.data[i][0]-x_a.data[i][0])*(x.data[i][0]-x_a.data[i][0]);
  }
  return erro/x.rows;
}

int main() {
  double tempo_reg, tempo_tri, erro_reg, erro_tri;
  system_clock::time_point inicio;

  for (int n = 100; n <= 1000; n+=100) {
    Matriz A(n, n, ZERO), b(n, 1, ZERO);
    Matriz x(n, 1, ZERO), x_a(n, 1, ZERO), y(n, 1, ZERO);
    Matriz L(n, n, ZERO), U(n, n, ZERO);

    init_sistema(A, b, n);
    init_solucao(x_a, n);

    printf("Para um sistema de dimensão %dx%d, temos:\n", n, n);

    inicio = system_clock::now();
    fatorar(A.copy(), L, U);
    resolver_triangular(L, y, b.copy(), LOWER);
    resolver_triangular(U, x, y, UPPER);
    tempo_reg = (double)(duration_cast<microseconds>(system_clock::now()-inicio).count());
    tempo_reg /= 1000;
    erro_reg = calcular_erro(x, x_a);
    printf("\tFatoração LU tradicional: tempo=%g ms, erro=%g\n", tempo_reg, erro_reg);

    inicio = system_clock::now();
    fatorar_tridiagonal(A.copy(), L, U);
    resolver_triangular(L, y, b.copy(), LOWER);
    resolver_triangular(U, x, y, UPPER);
    tempo_tri = (double)(duration_cast<microseconds>(system_clock::now()-inicio).count());
    tempo_tri /= 1000;
    erro_tri = calcular_erro(x, x_a);
    printf("\tFatoração LU tridiagonal: tempo=%g ms, erro=%g\n", tempo_tri, erro_tri);
  }

  return 0;
}
