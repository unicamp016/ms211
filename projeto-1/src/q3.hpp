/* Author: Rafael Figueiredo Prudencio */

#include <iostream>
#include <chrono>

#define REG 1
#define TRI 2
#define ZERO 3

#define UPPER 0
#define LOWER 1

using namespace std;
using namespace std::chrono;

class Matriz {
public:
  int rows;
  int cols;
  float **data;

  Matriz(int rows, int cols, int init=0) {
    this->rows = rows;
    this->cols = cols;

    /* Allocate memory for matrix */
    this->data = (float**) malloc(rows*sizeof(float*));
    for (int i = 0; i < rows; i++) {
      data[i] = (float*) malloc(cols*sizeof(float));
    }

    /* Initialize matrix with random values between [1, 10] */
    switch (init) {
      case REG:
        for (int i = 0; i < rows; i++) {
          for (int j = 0; j < cols; j++) {
            data[i][j] = rand()%10 + 1;
          }
        }
        break;
      case TRI:
        for (int i = 0; i < rows; i++) {
          for (int j = 0; j < cols; j++) {
            if (abs(i-j) > 1) continue;
            data[i][j] = rand()%10 + 1;
          }
        }
        break;
      case ZERO:
        for (int i = 0; i < rows; i++) {
          for (int j = 0; j < cols; j++) {
            data[i][j] = 0;
          }
        }
        break;
    }
  }

  void print() {
    for (int i = 0; i < this->rows; i++) {
      for (int j = 0; j < this->cols; j++) {
        cout << this->data[i][j] << " ";
      }
      cout << endl;
    }
    cout << endl;
  }

  void I() {
    if (this->rows != this->cols) return;
    for (int i = 0; i < this->rows; i++) {
      for (int j = 0; j < this->cols; j++) {
        if (i == j) this->data[i][j] = 1;
        else        this->data[i][j] = 0;
      }
    }
  }

  Matriz copy() {
    Matriz C(this->rows, this->cols);

    for (int i = 0; i < this->rows; i++) {
      for (int j = 0; j < this->cols; j++) {
        C.data[i][j] = this->data[i][j];
      }
    }
    return C;
  }
};
