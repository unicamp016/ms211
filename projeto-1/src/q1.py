# Author: Rafael Figueiredo Prudencio

import numpy as np

def findPrecision(type):
    A = type(1)
    s = type(2)
    while s > type(1):
        A = A/type(2)
        s = type(1) + A
    return type(2*A)

def main():
    single = findPrecision(np.float32)
    double = findPrecision(np.float64)
    print("Single float precision:", single)
    print("Double float precision:", double)

if __name__ == "__main__":
    main()
