/* Author: Lucas André */
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>

#define th 0.001
double f(float x){
  return (x-3)*(x-3)*(x-3)*(x+4);
}
int sinal(float x){
  if(x>0)
    return 1;
  else
    return 0;
}
double df(float x){
    return (-3 + x)*(-3 + x)*(9 + 4*x);
}
int main(){
  unsigned int i;
  float x0= 100,xk; // Variaveis para o método de newton
  float a=100,b=0,p; //Variáveis para o método da bisseção e da secante
  std::vector<float> bi,s,n,nm;
  //método da bisseção
  while(fabs(3-b)>th || fabs(3-a)>th){
    p=(a+b)/2;
    if(sinal(f(a))==sinal(f(p)))
      a=p;
    else
      b=p;
    bi.push_back(fabs(3-p));

  }
 a=100;
 b=0;
  //método da secante
  while(fabs(a-3)>th){
    p= (b*f(a) - a*f(b))/(f(a)-f(b));
    b=a;
    a=p;
    s.push_back(fabs(3-p));
  }
  //Método de newton padrão
  do{
    xk= x0 - f(x0)/df(x0);
    x0=xk;
    n.push_back(fabs(3-x0));
  }while(fabs(x0-3)>th);
  //Método de newton modificado
  x0=100;
  do{
    xk= x0 - 3*f(x0)/df(x0);
    x0=xk;
    nm.push_back(fabs(3-x0));
  }while(fabs(x0-3)>th);
  printf("E errolim bissecao secante newton newtonmod\n");
printf("%lu\n", bi.size());
  for(i=1;i<nm.size()+1;i++){
    printf("%i %f %f %f %f %f\n",i, th, bi[i-1], s[i-1], n[i-1], nm[i-1]);
  }
  for(;i<bi.size()+1;i++){
    printf("%i %f %f %f %f %f\n",i, th, bi[i-1], s[i-1], n[i-1], th);
  }
  for(;i<s.size()+1;i++){
    printf("%i %f %f %f %f %f\n",i, th, th, s[i-1], n[i-1], th);
  }
  for(;i<n.size()+1;i++){
    printf("%i %f %f %f %f %f\n",i, th, th, th, n[i-1], th);
  }


  return 0;
}
