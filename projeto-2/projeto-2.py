import math
import numpy as np
import matplotlib.pyplot as plt

params = {
    "T" : [100.0, 200.0],
    "N" : [2000.0, 200.0],
    "c1" : [1.1, 0.08],
    "d1" : [0.4, 0.09],
    "c2" : [0.4, 0.07],
    "d2" : [0.1, 0.075],
    "u1" : [10, 3],
    "u2" : [10, 1]
}

def lotka_volterra(c1, d1, c2, d2):
    prey = lambda u1, u2: c1*u1 - d1*u1*u2
    pred = lambda u1, u2: c2*u1*u2 - d2*u2
    return prey, pred

def euler(u1_i, u2_i, f_u1, f_u2, t0, N, T):
    dt = T/N
    n = int((t0+T)/dt)
    for _ in range(n+1):
        yield u1_i, u2_i
        t0 += dt
        u1_i = u1_i + dt*f_u1(u1_i, u2_i)
        u2_i = u2_i + dt*f_u2(u1_i, u2_i)

if __name__ == "__main__":
    sims = len(params["N"])
    for s in range(sims):
        t0 = 0
        N, T = params["N"][s], params["T"][s]
        c1, d1 = params["c1"][s], params["d1"][s]
        c2, d2 = params["c2"][s], params["d2"][s]
        u1_0, u2_0 = params["u1"][s], params["u2"][s]

        prey, pred = lotka_volterra(c1, d1, c2, d2)
        u_it = euler(u1_0, u2_0, prey, pred, t0, N, T)

        dt = T/N
        n = int((t0+T)/dt)
        t = [t*dt for t in range(n+1)]
        u1, u2 = [], []
        for u1_i, u2_i in u_it:
            u1.append(u1_i)
            u2.append(u2_i)

        print(t, u1, u2)
        plt.title(fr"População de presas ($u_1$) e predadores ($u_2$) em função do tempo")
        plt.ylabel("População de presas e predadores")
        plt.xlabel("Tempo (dias)")
        plt.plot(t, u1, label=r"Presas ($u_1$)")
        plt.plot(t, u2, label=r"Predadores ($u_2$)")
        plt.legend(loc='best')
        plt.show()
