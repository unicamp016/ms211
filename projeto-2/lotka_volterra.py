import math
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import itertools as itt

def euler(dt, u1_i, u2_i, f1, f2):
    # euler's method for the lotka volterra model
    u1 = u1_i + dt*f1(u1_i, u2_i) # u1_i+1
    u2 = u2_i + dt*f2(u1_i, u2_i) # u2_i+1
    return (u1, u2)

def lotka_volterra(T, N, u1_0, u2_0, c1, c2, d1, d2, uvt='u_vs_t', uvu='u_vs_u'):
    # u1_0 = u1(t_0) = u1(0) is the initial number of prey
    # u2_0 = u2(t_0) = u2(0) is the initial number of predators

    # c1 is the rate of prey population increase
    # d1 is the predation rate coefficient
    # c2 is the reproduction rate of predators per 1 prey eaten
    # d2 is the predator mortality rate

    # the interval [0, T] is divided in
    # N subintervals of size dt = T/N

    dt = T/N

    f1 = lambda u1, u2: c1*u1 - d1*u1*u2 # u1' = f1
    f2 = lambda u1, u2: c2*u1*u2 - d2*u2 # u2' = f2

    u1s = [u1_0] # u1_0, u1_1, ..., u1_N
    u2s = [u2_0] # u2_0, u2_1, ..., u2_N
    ts = [0] # t_0, t_1, ..., t_N where [t_0, t_N] = [0, T]

    u1_i, u2_i, t = u1_0, u2_0, 0
    for i in range(N):
        t += dt
        ts.append(t)
        # euler's method
        #  u1_i+1 = u1_i + dt*(c1*u1_i - d1*u1_i*u2_i)
        #  u2_i+1 = u2_i + dt*(c2*u1_i*u2_i - d2*u2_i)
        u1_i, u2_i = euler(dt, u1_i, u2_i, f1, f2)
        u1s.append(u1_i)
        u2s.append(u2_i)

    print(t, u1s, u2s)
    fig1 = plt.figure(figsize=(10, 10))
    ax1 = fig1.add_subplot(111)
    ax1.plot(ts, u1s) # plot u1 vs t
    ax1.plot(ts, u2s) # plot u2 vs t
    fig1.savefig(f'{uvt}.png')

    fig2 = plt.figure(figsize=(10, 10))
    ax2 = fig2.add_subplot(111)
    ax2.plot(u1s, u2s) # plot u2 vs u1
    fig2.savefig(f'{uvu}.png')

    return u1s, u2s, ts

lotka_volterra(T=200, N=200, u1_0=3, u2_0=1, c1=0.08, c2=0.07, d1=0.09, d2=0.075)
